do
	local COMMAND = {}
	COMMAND.arguments = ix.type.text

	function COMMAND:OnRun(client, message)
		local character = client:GetCharacter()
		local radios = character:GetInventory():GetItemsByUniqueID("handheld_radio", true)
		local item

		for k, v in ipairs(radios) do
			if (v:GetData("enabled", false)) then
				item = v
				break
			end
		end

		if (item) then
			if (!client:GetNetVar("restricted")) then
				ix.chat.Send(client, "radio", message)
				ix.chat.Send(client, "radio_eavesdrop", message)
			else
				return "@notNow"
			end
		elseif (#radios > 0) then
			return "@radioNotOn"
		else
			return "@radioRequired"
		end
	end

	ix.command.Add("Radio", COMMAND)
end

do
	local COMMAND = {}
	COMMAND.arguments = ix.type.number

	function COMMAND:OnRun(client, frequency)
		local character = client:GetCharacter()
		local inventory = character:GetInventory()
		local itemTable = inventory:HasItem("handheld_radio")

		if (itemTable) then
			--if (string.find(frequency, "^%d%d%d%.%d$")) then
				character:SetData("frequency", frequency)
				itemTable:SetData("frequency", frequency)

				client:Notify(string.format("Vous avez mis votre fréquence radio sur %s.", frequency))
			--end
		end
	end

	ix.command.Add("SetFreq", COMMAND)
end

do
	local COMMAND = {}

	function COMMAND:OnRun(client, arguments)
		netstream.Start(client, "ViewObjectives", Schema.CombineObjectives)
	end

	ix.command.Add("Pda", COMMAND)
end

do
	local COMMAND = {}

	function COMMAND:OnRun(client, arguments)
		local data = {}
			data.start = client:GetShootPos()
			data.endpos = data.start + client:GetAimVector() * 96
			data.filter = client
		local target = util.TraceLine(data).Entity

		if (IsValid(target) and target:IsPlayer() and target:GetNetVar("restricted")) then
			if (!client:GetNetVar("restricted")) then
				Schema:SearchPlayer(client, target)
			else
				return "@notNow"
			end
		end
	end

	ix.command.Add("CharSearch", COMMAND)
end

do
	local COMMAND = {}
	COMMAND.arguments = ix.type.text

	function COMMAND:OnRun(client, message)

			if (!client:GetNetVar("restricted")) then
				ix.chat.Send(client, "mel", message)
			else
				return "@notNow"
			end
	end

	ix.command.Add("Mel", COMMAND)
end

do
	local COMMAND = {}
	COMMAND.arguments = ix.type.text

	function COMMAND:OnRun(client, message)

			if (!client:GetNetVar("restricted")) then
				ix.chat.Send(client, "mec", message)
			else
				return "@notNow"
			end
	end

	ix.command.Add("Mec", COMMAND)
end

do
	local COMMAND = {}
	COMMAND.arguments = ix.type.text

	function COMMAND:OnRun(client, message)

			if (!client:GetNetVar("restricted")) then
				ix.chat.Send(client, "itl", message)
			else
				return "@notNow"
			end
	end

	ix.command.Add("Itl", COMMAND)
end

do
	local COMMAND = {}
	COMMAND.arguments = ix.type.text

	function COMMAND:OnRun(client, message)

			if (!client:GetNetVar("restricted")) then
				ix.chat.Send(client, "itc", message)
			else
				return "@notNow"
			end
	end

	ix.command.Add("Itc", COMMAND)
end