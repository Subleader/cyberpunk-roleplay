ITEM.name = "Drone de reconnaissance"
ITEM.model = "models/dronesrewrite/scout/scout.mdl"
ITEM.width = 3
ITEM.height = 2
ITEM.desc = "Utilisé par l'armée il survole la zone afin de trouver des infos."
ITEM.price = 0


ITEM.functions.use = { 
	name = "Poser",
	tip = "useTip",
	icon = "icon16/pencil.png",
	OnRun = function(item)
		local client = item.player
		local data = {}
			data.start = client:GetShootPos()
			data.endpos = data.start + client:GetAimVector()*96
			data.filter = client
		local trace = util.TraceLine(data)

		if (trace.HitPos) then
			local mine = ents.Create("dronesrewrite_scouty")
			mine:SetPos(trace.HitPos + trace.HitNormal * 10)
			mine:Spawn()
		end

		return true
	end,
}
