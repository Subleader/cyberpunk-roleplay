ITEM.name = "Grande Mine"
ITEM.model = "models/props/cs_office/cardboard_box03.mdl"
ITEM.width = 3
ITEM.height = 3
ITEM.desc = "Je ne jouerai pas avec ça si j'étais toi."
ITEM.price = 0


ITEM.functions.use = { 
	name = "Poser",
	tip = "useTip",
	icon = "icon16/pencil.png",
	OnRun = function(item)
		local client = item.player
		local data = {}
			data.start = client:GetShootPos()
			data.endpos = data.start + client:GetAimVector()*96
			data.filter = client
		local trace = util.TraceLine(data)

		if (trace.HitPos) then
			local mine = ents.Create("ent_jack_landmine_lrg")
			mine:SetPos(trace.HitPos + trace.HitNormal * 10)
			mine:Spawn()
		end

		return true
	end,
}
