ITEM.name = "Pulvérisateur"
ITEM.description = "Un marteau avec un canon qui s'active au moment de l'impacte, très pratique pour exploser des cranes."
ITEM.model = "models/weapons/tfa_kf2/w_pulverizer.mdl"
ITEM.class = "tfa_kf2_pulverizer"
ITEM.weaponCategory = "melee"
ITEM.width = 4
ITEM.height = 1