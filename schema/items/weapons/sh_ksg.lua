ITEM.name = "KSG (Calibre 12)"
ITEM.description = "Le Kel-Tec KSG-12 est un fusil de combat rapproché (fusil à pompe) qui utilise des cartouches de calibre 12, conçu en 2011 par George Kellgren (en) pour Kel-Tec (en) CNC Industries Inc. Le KSG-12 est une arme de type bullpup (raccourcie en longueur), avec deux tubes de logement de cartouches que l'utilisateur peut changer en cours d'utilisation. La sélection du tube se fait par la manipulation d'un levier à trois positions, la position centrale correspondant à la sécurité, les deux autres permettant de recharger l'arme depuis un tube ou l'autre. Cela permet une capacité bien supérieure aux armes à chargeur tubulaire puisque ses deux chargeurs lui permettent une capacité de 14 cartouches (2 tubes = 2x7 cartouches)."
ITEM.model = "models/weapons/tfa_ins2/w_ksg.mdl"
ITEM.class = "tfa_ins2_ksg"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}