ITEM.name = "CZ 805 (5.56x45mm)"
ITEM.description = "Le CZ-805 BREN est un fusil d'assaut tchèque fabriqué par Česká Zbrojovka (CZUB) et qui remplace depuis 2011 le Sa Vz 58 au sein des Forces armées tchèques. Ce fusil d'assaut est fabriqué en acier (culasse/canon), alliage léger (carcasse) et matériaux composites (monture et canon) et fonctionne par piston et verrou rotatif."
ITEM.model = "models/weapons/tfa_ins2/w_cz805.mdl"
ITEM.class = "tfa_ins2_cz805"
ITEM.weaponCategory = "primary"
ITEM.width = 4
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}