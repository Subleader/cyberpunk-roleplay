ITEM.name = "ACR (5.56x45mm)"
ITEM.description = "L'ACR (originellement dénommé Masada) est un fusil d'assaut américain créé en 2006 par Magpul. Magpul apporta plusieurs améliorations au Masada, tant sur le plan de l'ergonomie que de la fiabilité et fut rebaptisé ACR (Adaptative Combat Rifle). Magpul s'est associé à Bushmaster et à Remington pour la production de l'ACR."
ITEM.model = "models/weapons/tfa_ins2/w_acr.mdl"
ITEM.class = "tfa_ins2_acr"
ITEM.weaponCategory = "primary"
ITEM.width = 5
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}