ITEM.name = "AK 400 (7.62x39mm)"
ITEM.description = "L’AK-400 est essentiellement un AK-74 très fortement modernisé, avec de nouveaux ajouts comme une crosse pliante et télescopique, des rails Picatinny, une nouvelle poignée pour pistolet contenant le kit de nettoyage, une gâchette repensée, un régulateur de gaz à deux positions et un piston à gaz à course courte qui pousse entre autres sur un boulon assez standard à motif AK."
ITEM.model = "models/weapons/tfa_ins2/w_ak400.mdl"
ITEM.class = "tfa_ins2_ak400"
ITEM.weaponCategory = "primary"
ITEM.width = 5
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}