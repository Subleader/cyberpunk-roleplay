ITEM.name = "Glock 18 SAI (9mm)"
ITEM.description = "Le Glock 18 est un pistolet fabriqué par l'entreprise autrichienne Glock. Le G18 est de couleur noire mate. Il possède des rayures de maintien placées à l'avant et à l'arrière de la crosse, un pontet rectangulaire se terminant en pointe sur sa partie basse et, à l'avant, la hausse et mire fixe."
ITEM.model = "models/weapons/tfa_ins2/w_g18.mdl"
ITEM.class = "tfa_ins2_g18"
ITEM.weaponCategory = "secondary"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}