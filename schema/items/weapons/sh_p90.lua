ITEM.name = "FN P90 (5.7x28mm)"
ITEM.description = "Le FN P90 est un pistolet mitrailleur de conception belge fabriqué par la FN Herstal. Cette arme présente des caractéristiques techniques novatrices par rapport aux autres fusils d'assaut, notamment sa petite taille et sa très forte cadence de tir."
ITEM.model = "models/weapons/tfa_ins2/w_p90.mdl"
ITEM.class = "tfa_ins2_p90"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}