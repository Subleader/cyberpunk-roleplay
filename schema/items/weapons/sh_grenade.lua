ITEM.name = "Grenade à fragmentation"
ITEM.description = "Une grenade anti-personnel très efficace sur une petite zone d'effet."
ITEM.model = "models/weapons/w_grenade.mdl"
ITEM.class = "weapon_frag"
ITEM.weaponCategory = "grenade"
ITEM.width = 1
ITEM.height = 1