ITEM.name = "Controlleur de Drone"
ITEM.description = "Permet de controller un drone à distance"
ITEM.model = "models/dronesrewrite/jammer/w_jammer.mdl"
ITEM.class = "weapon_drr_remote"
ITEM.weaponCategory = "secondary"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}