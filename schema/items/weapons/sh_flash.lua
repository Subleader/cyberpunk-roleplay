ITEM.name = "Grenade flash"
ITEM.description = "Une grenade aveuglante très efficace sur une petite zone d'effet."
ITEM.model = "models/weapons/tfa_csgo/w_flash.mdl"
ITEM.class = "tfa_csgo_flash"
ITEM.weaponCategory = "grenade"
ITEM.width = 1
ITEM.isGrenade = true
ITEM.height = 1