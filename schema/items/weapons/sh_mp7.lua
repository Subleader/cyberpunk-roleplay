ITEM.name = "HK MP7 (4,6x30mm)"
ITEM.description = "Le MP7 est un pistolet-mitrailleur répondant au principe d’Advanced Personal Defense Weapon (APDW) qui vise à combler le fossé entre le pistolet mitrailleur conventionnel et le fusil d'assaut. Les PDW sont destinés à doter les troupes qui ne sont pas directement employées dans les combats d’une capacité de riposte significative. Les officiers, équipages de chars ou servants d’artillerie n’ont pas besoin de s’encombrer d’un fusil d’assaut mais doivent néanmoins pouvoir faire face à une attaque inopinée et défaire des adversaires équipés de vestes pare-éclats en mesure de stopper une munition d’arme de poing."
ITEM.model = "models/weapons/w_smg_blast_lynx_mp7.mdl"
ITEM.class = "tfa_blast_lynx_mp7"
ITEM.weaponCategory = "primary"
ITEM.width = 2
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}