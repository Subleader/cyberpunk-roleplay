ITEM.name = "Couteau de combat"
ITEM.description = "Un couteau de combat est un couteau conçu pour l'usage militaire, spécialement pour le combat rapproché. En tant que couteau, il s'agit d'une arme blanche dont le port est réglementé."
ITEM.model = "models/weapons/tfa_ins2/w_marinebayonet.mdl"
ITEM.class = "tfa_ins2_kabar"
ITEM.weaponCategory = "melee"
ITEM.width = 2
ITEM.height = 1