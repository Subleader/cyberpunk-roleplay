ITEM.name = "HK USP 45"
ITEM.description = "L’USP est un pistolet semi-automatique double action de la société allemande Heckler & Koch GmbH (H&K) de Oberndorf am Neckar, apparu en 1993 pour remplacer le pistolet P7. Le Universal Selbstlade Pistole, ou pistolet semi-automatique universel est chambré pour différents calibres et disponible en de nombreuses versions."
ITEM.model = "models/weapons/tfa_ins2/w_usp45.mdl"
ITEM.class = "tfa_ins2_usp45"
ITEM.weaponCategory = "primary"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}