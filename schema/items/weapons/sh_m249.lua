ITEM.name = "M249 (5.56x45mm)"
ITEM.description = "Cette mitrailleuse est chambrée en 5.56 mm OTAN, plus spécifiquement la cartouche « ss109 » ayant été initialement créée pour elle. Cette munition de 5,56 mm, employée normalement dans les fusils d'assaut, est moins puissante et porte donc moins loin que les munitions généralement utilisées dans les mitrailleuses moyennes, mais elle est aussi moins encombrante et permet donc d'avoir une arme plus mobile pouvant être maniée par un seul homme. De plus, elle est dotée d'une importante cadence de tir, qui lui permet de procurer un appui efficace, du point de vue défensif et offensif."
ITEM.model = "models/weapons/tfa_ins2/w_minimi.mdl"
ITEM.class = "tfa_ins2_minimi"
ITEM.weaponCategory = "primary"
ITEM.width = 5
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}