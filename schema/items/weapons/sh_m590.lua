ITEM.name = "M590 (Calibre 12)"
ITEM.description = "Le Mossberg 500 est un fusil à répétition ou Slide Action, en argot  à pompe  à ârme lisse conçu aux États-Unis par la firme O.F. Mossberg & Sons. Cette arme est l'une des plus répandue sur le marché américain aux côtés du Winchester 1300 et du Remington 870 en raison de sa bonne qualité et de son prix raisonnable. Il existe une copie pakistanaise de qualité de la version police de cette arme fabriqué par Daudsons Armoury (Peshawar)."
ITEM.model = "models/weapons/tfa_ins2/w_m590_olli.mdl"
ITEM.class = "tfa_ins2_m590o"
ITEM.weaponCategory = "primary"
ITEM.width = 5
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}