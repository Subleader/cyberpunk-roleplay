ITEM.name = "Grenade fumigène"
ITEM.description = "Une grenade qui créee une zone de fumée très efficace sur une petite zone d'effet."
ITEM.model = "models/weapons/tfa_csgo/w_smoke.mdl"
ITEM.class = "tfa_csgo_smoke"
ITEM.weaponCategory = "grenade"
ITEM.isGrenade = true
ITEM.width = 1
ITEM.height = 1