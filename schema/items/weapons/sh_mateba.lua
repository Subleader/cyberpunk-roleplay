ITEM.name = "Mateba (44 magnum)"
ITEM.description = "Le Mateba Model 6 Unica (souvent appelé simplement Mateba ou Mateba Autorevolver) est un revolver semi-automatique à rappel, un des rares de ce type jamais produit. Il a été développé par Mateba, basé à Pavia, en Italie. Emilio Ghisoni (décédé en 2008) est le propriétaire du brevet américain 4 712 466, qui détaille le fonctionnement de l'arme. Le 23 mars 2018, Mateba à Montebelluna, en Italie, a réintroduit les 6 Unica et Grifone."
ITEM.model = "models/weapons/tfa_ins2/w_mateba.mdl"
ITEM.class = "tfa_ins2_mateba"
ITEM.weaponCategory = "secondary"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}