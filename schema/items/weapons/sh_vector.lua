ITEM.name = "Vector (45 acp)"
ITEM.description = "Le KRISS Vector aussi appelé TDI KRISS Super V XSMG est un pistolet mitrailleur chambré en .45 ACP, en 9 mm et en .40 S&W fabriqué par Transformational Defense Industries en 2006 sur la base des brevets déposés par Renaud Kerbrat en 2003. L'énergie du recul est absorbée grâce à l'utilisation d'une masse mobile se déplaçant vers le bas lors du mouvement de la culasse vers l'arrière sous l'action des gaz, cette masse exerçant une action opposée au relèvement en étant liée à la culasse par une biellette dont l'angle est calculé pour obtenir une vitesse de déplacement et une amplitude précises, la résultante étant inversement proportionnelle au déplacement de l'arme dû au recul. Le canon est aussi aligné avec la main et l'épaule du tireur. Combinés, ces facteurs permettent de mieux maîtriser le recul et le relèvement de l'arme, notamment en tir automatique."
ITEM.model = "models/weapons/tfa_ins2/w_krissv.mdl"
ITEM.class = "tfa_ins2_krissv"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}