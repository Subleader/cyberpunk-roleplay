ITEM.name = "Famas F1 (5.56x45mm)"
ITEM.description = "Le FAMAS (ou FA-MAS) Note 1, désigné comme « fusil d'assaut1 de 5,56 mm modèle F1 MAS » au sein de l’armée française, est un fusil d'assaut français de calibre 5,56 × 45 mm Otan1 de type bullpup, initialement fabriqué par la Manufacture d'armes de Saint-Étienne."
ITEM.model = "models/weapons/tfa_ins2/w_famas.mdl"
ITEM.class = "tfa_ins2_famas"
ITEM.weaponCategory = "primary"
ITEM.width = 4
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}