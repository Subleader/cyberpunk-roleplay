ITEM.name = "VC30 (Calibre 12)"
ITEM.description = "Le VC30 est un fusil à pompe fabriqué par la société Kelect aux Etats-Unis, il s'agit d'un fusil capable de tirer deux coups en semi-auto avant de devoir rechambrer une cartouche."
ITEM.model = "models/weapons/w_tfa_vc30.mdl"
ITEM.class = "tfa_kzsf_vc30"
ITEM.weaponCategory = "primary"
ITEM.width = 4
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}