ITEM.name = "Grenade incendiaire"
ITEM.description = "Une grenade qui créee une zone de feu très efficace sur une petite zone d'effet."
ITEM.model = "models/weapons/tfa_csgo/w_incend.mdl"
ITEM.class = "tfa_csgo_incen"
ITEM.weaponCategory = "grenade"
ITEM.isGrenade = true
ITEM.width = 1
ITEM.height = 1