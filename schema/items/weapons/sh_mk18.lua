ITEM.name = "MK18 (5.56x45mm)"
ITEM.description = "La carabine d'assaut Mk 18 Mod. 0 est une variante plus compacte du Colt M4 en service dans l'US Navy depuis la fin des années 2000."
ITEM.model = "models/weapons/tfa_ins2/w_mk18mod.mdl"
ITEM.class = "tfa_ins2_mk18mod"
ITEM.weaponCategory = "primary"
ITEM.width = 4
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}