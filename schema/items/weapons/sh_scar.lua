ITEM.name = "Scar H (7.62x51mm)"
ITEM.description = "Le FN SCAR (Special Operations Forces Combat Assault Rifle) est un fusil d'assaut de conception belge fabriqué par la FN Herstal. Le SCAR se présente en 3 versions,Le SCAR P ; une version PM (pistolet mitrailleur) qui ressemble beaucoup au MP5 d'ailleurs, SCAR-L (pour Light) appelé Mk-16 par les forces armées des États-Unis chambré en 5,56 mm OTAN et le SCAR-H (pour Heavy) (Mk-17) chambré en 7,62 x 51 mm OTAN."
ITEM.model = "models/weapons/tfa_ins2/w_scarl.mdl"
ITEM.class = "tfa_ins2_scarl"
ITEM.weaponCategory = "primary"
ITEM.width = 5
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}