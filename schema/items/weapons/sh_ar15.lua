ITEM.name = "AR15 (5,56x45mm)"
ITEM.description = "L'AR-15, dont la désignation militaire adoptée aux États-Unis est « M16 », est un fusil d'assaut inventé par l'Américain Eugene M. Stoner. Contrairement à ce qui pourrait être pensé, dans la désignation « AR-15 » le sigle « AR » ne fait pas référence à assault rifle (en anglais, « fusil d'assaut ») mais aux deux premières lettres du nom de la société qui a en premier conçu le fusil : ArmaLite. Cette dernière en développa le prototype pour ensuite le vendre à la société Colt. À partir de 1963, le nom AR-15 définit les fusils semi-automatiques et fusils automatiques dérivés du célèbre fusil d'assaut vendus par Colt. À partir des années 1980, son fabricant l'a décliné en AR-15 Carbine puis Colt Sporter Lighweight, issues elles-mêmes des carabines militaires Colt Commando et M4. Les armes semi-automatiques reprenant sa forme et sa mécanique sont innombrables (tir semi-automatique par emprunt de gaz et culasse rotative)."
ITEM.model = "models/weapons/w_rif_m4a1.mdl"
ITEM.class = "tfa_ins2_cw_ar15"
ITEM.weaponCategory = "primary"
ITEM.width = 5
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}