ITEM.name = "FN Five Seven (5.7x28mm)"
ITEM.description = "Le FN Five-seveN est un pistolet semi-automatique simple action ou double action de conception belge fabriqué par la FN Herstal. Il est chambré pour une munition très particulière puisqu'il s'agit de celle utilisée dans le P90 dont il est l'arme secondaire toute désignée. La munition, 5,7 x 28 mm est conçue comme une version miniature du 5,56 mm OTAN, bien moins puissante elle permet toutefois de passer au travers des protections individuelles. Le Five-seveN est construit autour d'une carcasse en polymère et possède un rail sous le canon permettant d'installer des accessoires tels qu'une torche ou un viseur laser."
ITEM.model = "models/weapons/tfa_ins2/w_fn57.mdl"
ITEM.class = "tfa_ins2_fiveseven"
ITEM.weaponCategory = "secondary"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}