ITEM.name = "SR25 (7.62x51mm)"
ITEM.description = "Le SR-25 (Stoner Rifle-25) est un fusil de précision semi-automatique de calibre 7,62 × 51 mm Otan conçu par Eugene M. Stoner et fabriqué par la société d'armement Knight. Le SR-25 utilise une culasse rotative et un système d'emprunt de gaz direct semblable à celui des M16/M4. Il est vaguement basé sur le Stoner AR-10, rechambré dans son calibre original de 7.62x51mm OTAN."
ITEM.model = "models/weapons/tfa_ins2/w_sr25.mdl"
ITEM.class = "tfa_ins2_sr25"
ITEM.weaponCategory = "primary"
ITEM.width = 6
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.70499622821808, 268.25439453125, 0),
	fov	= 16,
	pos	= Vector(0, 200, -3)
}