
-- You can define attributes in the attributes/ folder. The unique ID of the attribute will be the same as the name of the file.

ATTRIBUTE.name = "Entretien"
ATTRIBUTE.description = "Affecte votre capacité à entretenir votre armure et vos armes à feu."