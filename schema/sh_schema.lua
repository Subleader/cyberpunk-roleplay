
-- The shared init file. You'll want to fill out the info for your schema and include any other files that you need.

-- Schema info
Schema.name = "Cyberpunk Roleplay"
Schema.author = "Subleader"
Schema.description = "Les cendres sont éparpillées, il faut les retrouver."

-- Additional files that aren't auto-included should be included here. Note that ix.util.Include will take care of properly
-- using AddCSLuaFile, given that your files have the proper naming scheme.

-- You could technically put most of your schema code into a couple of files, but that makes your code a lot harder to manage -
-- especially once your project grows in size. The standard convention is to have your miscellaneous functions that don't belong
-- in a library reside in your cl/sh/sv_schema.lua files. Your gamemode hooks should reside in cl/sh/sv_hooks.lua. Logical
-- groupings of functions should be put into their own libraries in the libs/ folder. Everything in the libs/ folder is loaded
-- automatically.
ix.util.Include("libs/thirdparty/sh_netstream2.lua")

ix.util.Include("sh_commands.lua")

ix.util.Include("sh_configs.lua")
ix.util.Include("cl_schema.lua")
ix.util.Include("cl_hooks.lua")
ix.util.Include("sv_schema.lua")
ix.util.Include("sh_hooks.lua")
ix.util.Include("sv_hooks.lua")

-- You'll need to manually include files in the meta/ folder, however.
ix.util.Include("meta/sh_character.lua")
ix.util.Include("meta/sh_player.lua")


--ix.anim.SetModelClass("models/stalkertnb/sunrise_ana2.mdl", "citizen_female")
player_manager.AddValidModel( "Hunk",	"models/auditor/re2/chr_hunk_npc_f.mdl" )
player_manager.AddValidHands( "Hunk",	"models/auditor/re2/c_hunk_viewhand.mdl", 0, "00000000" )

function Schema:BuildBusinessMenu()
	return false
end



do
	local CLASS = {}
	CLASS.color = Color(75, 150, 50)
	CLASS.format = "%s dit par radio \"%s\""

	function CLASS:CanHear(speaker, listener)
		local character = listener:GetCharacter()
		local inventory = character:GetInventory()

		for k, v in pairs(inventory:GetItemsByUniqueID("handheld_radio", true)) do
			if (v:GetData("enabled", false)) then
				return speaker:GetCharacter():GetData("frequency") == character:GetData("frequency")
			end
		end
	end

	ix.chat.Register("radio", CLASS)
end

do
	local CLASS = {}
	CLASS.color = Color(255, 255, 175)
	CLASS.format = "%s dit par radio \"%s\""

	function CLASS:GetColor(speaker, text)
		if (LocalPlayer():GetEyeTrace().Entity == speaker) then
			return Color(175, 255, 175)
		end

		return self.color
	end

	function CLASS:CanHear(speaker, listener)
		if (ix.chat.classes.radio:CanHear(speaker, listener)) then
			return false
		end

		local chatRange = ix.config.Get("chatRange", 280)

		return (speaker:GetPos() - listener:GetPos()):LengthSqr() <= (chatRange * chatRange)
	end
	ix.chat.Register("radio_eavesdrop", CLASS)
end

do
	local CLASS = {}
	CLASS.color = Color(255, 255, 150)
	CLASS.format = "**** %s %s"

	function CLASS:GetColor(speaker, text)
		if (LocalPlayer():GetEyeTrace().Entity == speaker) then
			return Color(175, 255, 150)
		end

		return self.color
	end

	function CLASS:CanHear(speaker, listener)

		local chatRange = ix.config.Get("chatRange", 280)*3
		return (speaker:GetPos() - listener:GetPos()):LengthSqr() <= (chatRange * chatRange)
	end

	ix.chat.Register("mel", CLASS)
end

do
	local CLASS = {}
	CLASS.color = Color(255, 255, 150)
	CLASS.format = "* %s %s"

	function CLASS:GetColor(speaker, text)
		if (LocalPlayer():GetEyeTrace().Entity == speaker) then
			return Color(175, 255, 150)
		end

		return self.color
	end

	function CLASS:CanHear(speaker, listener)

		local chatRange = ix.config.Get("chatRange", 280)/3
		return (speaker:GetPos() - listener:GetPos()):LengthSqr() <= (chatRange * chatRange)
	end

	ix.chat.Register("mec", CLASS)
end

do
	local CLASS = {}
	CLASS.color = Color(255, 255, 150)
	CLASS.format = "* %s"

	function CLASS:OnChatAdd(speaker, text)
		chat.AddText(self.color, string.format(self.format, text))
	end
	
	function CLASS:CanHear(speaker, listener)

		local chatRange = ix.config.Get("chatRange", 280)/3
		return (speaker:GetPos() - listener:GetPos()):LengthSqr() <= (chatRange * chatRange)
	end

	ix.chat.Register("itc", CLASS)
end

do
	local CLASS = {}
	CLASS.color = Color(255, 255, 150)
	CLASS.format = "**** %s"

	function CLASS:OnChatAdd(speaker, text)
		chat.AddText(self.color, string.format(self.format, text))
	end
	
	function CLASS:CanHear(speaker, listener)

		local chatRange = ix.config.Get("chatRange", 280)*3
		return (speaker:GetPos() - listener:GetPos()):LengthSqr() <= (chatRange * chatRange)
	end

	ix.chat.Register("itl", CLASS)
end