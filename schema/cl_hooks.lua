
-- Here is where all of your clientside hooks should go.

-- Disables the crosshair permanently.
function Schema:PopulateCharacterInfo(client, character, tooltip)
	if (client:GetNetVar("restricted")) then
		local panel = tooltip:AddRowAfter("name", "ziptie")
		panel:SetBackgroundColor(derma.GetColor("Warning", tooltip))
		panel:SetText(L("tiedUp"))
		panel:SizeToContents()
	elseif (client:GetNetVar("tying")) then
		local panel = tooltip:AddRowAfter("name", "ziptie")
		panel:SetBackgroundColor(derma.GetColor("Warning", tooltip))
		panel:SetText(L("beingTied"))
		panel:SizeToContents()
	elseif (client:GetNetVar("untying")) then
		local panel = tooltip:AddRowAfter("name", "ziptie")
		panel:SetBackgroundColor(derma.GetColor("Warning", tooltip))
		panel:SetText(L("beingUntied"))
		panel:SizeToContents()
	end
end

function Schema:CharacterLoaded(character)
	self:ExampleFunction("@serverWelcome", character:GetName())
end

hook.Add("HUDPaint", "HUD_Armor",function ()
	if (LocalPlayer():GetNetVar("gasmask") == true) then		
		if (LocalPlayer():Health() <= 20) then
			surface.SetDrawColor(255,255,255,255)
			surface.SetMaterial(Material("hud/bron4.png"))
			surface.DrawTexturedRect( ScrW()/1.05, ScrH()/1.35,  33, 35) --Icone clignement			
		elseif (LocalPlayer():Health() <= 40) then
			surface.SetDrawColor(255,255,255,255)
			surface.SetMaterial(Material("hud/bron3.png"))
			surface.DrawTexturedRect( ScrW()/1.05, ScrH()/1.35,  33, 35) --Icone clignement	
		elseif (LocalPlayer():Health() <= 60) then
			surface.SetDrawColor(255,255,255,255)
			surface.SetMaterial(Material("hud/bron2.png"))
			surface.DrawTexturedRect( ScrW()/1.05, ScrH()/1.35,  33, 35) --Icone clignement		
		elseif (LocalPlayer():Health() <= 80) then
			surface.SetDrawColor(255,255,255,255)
			surface.SetMaterial(Material("hud/bron.png"))
			surface.DrawTexturedRect( ScrW()/1.05, ScrH()/1.35,  33, 35) --Icone clignement	
		end
	end
end )

hook.Add("HUDPaint", "HUD_Sang",function ()
	if (LocalPlayer():GetCharacter()) then
		if (LocalPlayer():Health() <= 20) then
			surface.SetDrawColor(200,200,200,255)
			surface.SetMaterial(Material("clockwork/screendamage.png"))
			surface.DrawTexturedRect( 0, 0,  ScrW(), ScrH()) --Icone clignement	
		elseif (LocalPlayer():Health() <= 40) then
			surface.SetDrawColor(200,200,200,200)
			surface.SetMaterial(Material("clockwork/screendamage.png"))
			surface.DrawTexturedRect( 0, 0,  ScrW(), ScrH()) --Icone clignement	
		elseif (LocalPlayer():Health() <= 60) then
			surface.SetDrawColor(200,200,200,150)
			surface.SetMaterial(Material("clockwork/screendamage.png"))
			surface.DrawTexturedRect( 0, 0,  ScrW(), ScrH()) --Icone clignement		
		elseif (LocalPlayer():Health() <= 80) then
			surface.SetDrawColor(200,200,200,100)
			surface.SetMaterial(Material("clockwork/screendamage.png"))
			surface.DrawTexturedRect( 0, 0,  ScrW(), ScrH()) --Icone clignement	
		end
	end
end)


netstream.Hook("Frequency", function(oldFrequency)
	Derma_StringRequest("Frequency", "What would you like to set the frequency to?", oldFrequency, function(text)
		ix.command.Send("SetFreq", text)
	end)
end)

netstream.Hook("ViewObjectives", function(data)
	vgui.Create("ixViewObjectives"):Populate(data)
end)