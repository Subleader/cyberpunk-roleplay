
-- Here is where all of your shared hooks should go.

-- Disable entity driving.
function Schema:CanDrive(client, entity)
	return false
end

function Schema:CanPlayerViewObjectives(client)
	return client:IsCombine()
end

function Schema:CanPlayerEditObjectives(client)
	local bCanEdit = false
	local name = client:GetCharacter():GetName()

	if (client:IsAdmin()) then
		bCanEdit = true
	end

	return bCanEdit
end