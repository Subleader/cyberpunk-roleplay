hook.Add("LoadFonts", "ixCombineViewObjectives", function()
	surface.CreateFont("ixCombineViewObjectives", {
		font = "Courier New",
		size = 16,
		antialias = true,
		weight = 400
	})
end)

DEFINE_BASECLASS("DFrame")

local PANEL = {}

local animationTime = 1
AccessorFunc(PANEL, "bCommitOnClose", "CommitOnClose", FORCE_BOOL)

function PANEL:Init()
	self:SetCommitOnClose(true)
	self:SetBackgroundBlur(true)
	self:ShowCloseButton( false )
	self:SetSize(ScrW() / 5 > 200 and ScrW() / 5 or ScrW() / 3, ScrH() / 2 > 300 and ScrH() / 2 or ScrH())
	self:Center()
		
	self.textEntry = vgui.Create("DTextEntry", self)
	self.textEntry:SetMultiline(true)
	self.textEntry:SetPaintBackgroundEnabled(true)
	self.textEntry:SetPos(36,51)
	self.textEntry:SetSize(ScrW() / 6 > 200 and ScrW() / 6 or ScrW() / 4, ScrH() / 2.54 > 300 and ScrH() / 2.54 or ScrH())
	self.textEntry:SetFont("ixCombineViewObjectives")
	
	self.myButton = vgui.Create("DButton", self)
	self.myButton:SetPaintBackgroundEnabled(true)
	self.myButton:SetText( "Fermer" )
	self.myButton:SetPos( 36, 30)
	self.myButton:SetSize(320,20)
	self.myButton.DoClick = function() self:Close() end
	self.myButton.DoClick = function() self:Close() end
end

function PANEL:ApplySchemeSettings()
	self.myButton:SetBGColor( Color(255,125,125,150))
	self.textEntry:SetBGColor( Color(180,180,180))
	self.textEntry:SetAlpha( 200 )
end

function PANEL:Populate(data, bDontShow)
	data = data or {}

	self.oldText = data.text or ""
	self.alpha = 255
	self:SetTitle(L(""))
	self.textEntry:SetText(data.text or "")

	if (!hook.Run("CanPlayerEditObjectives", LocalPlayer())) then
		self.textEntry:SetEnabled(false)
	end

	if (!bDontShow) then
		self.alpha = 0
		self:SetAlpha(0)
		self:MakePopup()

		self:CreateAnimation(animationTime, {
			index = 1,
			target = {alpha = 255},
			easing = "outQuint",

			Think = function(animation, panel)
				panel:SetAlpha(panel.alpha)
			end
		})
	end
end

function PANEL:Paint( w, h )
	surface.SetDrawColor(255,255,255,255)
    surface.SetMaterial(Material("gui/pda.png"))
    surface.DrawTexturedRect( 0, -20, ScrW() / 5 > 200 and ScrW() / 5 or ScrW() / 3, ScrH() / 2 > 300 and ScrH() / 2 or ScrH())
end

function PANEL:CommitChanges()
	local text = string.Trim(self.textEntry:GetValue():sub(1, 2000))

	-- only update if there's something different so we can preserve the last editor if nothing changed
	if (self.oldText != text) then
		netstream.Start("ViewObjectivesUpdate", text)
	end
end

function PANEL:Close()
	if (self.bClosing) then
		return
	end

	self.bClosing = true

	if (self:GetCommitOnClose()) then
		self:CommitChanges()
	end

	self:SetMouseInputEnabled(false)
	self:SetKeyboardInputEnabled(false)

	self:CreateAnimation(animationTime, {
		target = {alpha = 0},
		easing = "outQuint",

		Think = function(animation, panel)
			panel:SetAlpha(panel.alpha)
		end,

		OnComplete = function(animation, panel)
			BaseClass.Close(panel)
		end
	})
end

vgui.Register("ixViewObjectives", PANEL, "DFrame")