
-- You can define factions in the factions/ folder. You need to have at least one faction that is the default faction - i.e the
-- faction that will always be available without any whitelists and etc.

FACTION.name = "Afterlife"
FACTION.description = "Placebo."
FACTION.isDefault = true
FACTION.color = Color(25, 9, 146)
FACTION.models = {
	"models/kss/tsremastered/smod_operator_tac_01_npc_e.mdl",
	"models/kss/tsremastered/smod_operator_tac_02_npc_e.mdl",
	"models/kss/tsremastered/smod_tactical_soldier_npc_e.mdl",
	"models/pmc_combine_soldier_prisonguard.mdl",
	"models/pmc_combine_super_soldier.mdl"

}

function FACTION:OnCharacterCreated(client, character)
	local inventory = character:GetInventory()
	inventory:Add("bandage", 2)
	inventory:Add("water", 1)
end

FACTION_AFTERLIFE = FACTION.index
