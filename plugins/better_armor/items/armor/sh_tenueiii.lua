ITEM.name = "Gilet pare-balles III"
ITEM.description = "Un gilet pare-balles protégeant des calibres intermédiaires, l'un des plus communs, allant jusqu'au 7.62x39."
ITEM.model = "models/weapons/armor/armor.mdl"
ITEM.width = 3
ITEM.armorAmount = 0
ITEM.gasmask = false -- It will protect you from bad air
ITEM.height = 3
ITEM.category = "armor"
ITEM.resistance = true -- This will activate the protection bellow
ITEM.damage = { -- It is scaled; so 100 damage * 0.8 will makes the damage be 80.
			0.65, -- Bullets
			0.65, -- Slash
			0.65, -- Shock
			0.65, -- Burn
			0.65, -- Radiation
			0.65, -- Acid
			0.65, -- Explosion
}