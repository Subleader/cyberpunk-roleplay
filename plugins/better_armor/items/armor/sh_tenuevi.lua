ITEM.name = "Gilet pare-balles VI"
ITEM.description = "Un gilet pare-balles nouvelle technologie protégeant de la totalité des calibres superieurs ainsi que des balles accélérées, un gilet chers et rare, très efficace et plutot léger pour sa protection, allant jusqu'au 338."
ITEM.model = "models/weapons/armor/armor.mdl"
ITEM.width = 3
ITEM.armorAmount = 0
ITEM.gasmask = false -- It will protect you from bad air
ITEM.height = 3
ITEM.category = "armor"
ITEM.resistance = true -- This will activate the protection bellow
ITEM.damage = { -- It is scaled; so 100 damage * 0.8 will makes the damage be 80.
			0.40, -- Bullets
			0.40, -- Slash
			0.40, -- Shock
			0.40, -- Burn
			0.40, -- Radiation
			0.40, -- Acid
			0.40, -- Explosion
}