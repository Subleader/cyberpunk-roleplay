ITEM.name = "Gilet pare-balles V"
ITEM.description = "Un gilet pare-balles protégeant de la totalité des calibres superieurs, un gilet chers, très efficace et lourd, allant jusqu'au 308 ou encore le 7.62x54r."
ITEM.model = "models/weapons/armor/armor.mdl"
ITEM.width = 3
ITEM.armorAmount = 0
ITEM.gasmask = false -- It will protect you from bad air
ITEM.height = 3
ITEM.category = "armor"
ITEM.resistance = true -- This will activate the protection bellow
ITEM.damage = { -- It is scaled; so 100 damage * 0.8 will makes the damage be 80.
			0.50, -- Bullets
			0.50, -- Slash
			0.50, -- Shock
			0.50, -- Burn
			0.50, -- Radiation
			0.50, -- Acid
			0.50, -- Explosion
}