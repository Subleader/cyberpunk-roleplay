ITEM.name = "Gilet pare-balles II"
ITEM.description = "Un gilet pare-balles protégeant des petits calibres, l'un des plus légers mais aussi moins résistants gilets allant jusqu'au 9mm acp ou 357 magnum de basse qualité."
ITEM.model = "models/weapons/armor/armor.mdl"
ITEM.width = 3
ITEM.armorAmount = 0
ITEM.gasmask = false -- It will protect you from bad air
ITEM.height = 3
ITEM.category = "armor"
ITEM.resistance = true -- This will activate the protection bellow
ITEM.damage = { -- It is scaled; so 100 damage * 0.8 will makes the damage be 80.
			0.8, -- Bullets
			0.8, -- Slash
			0.8, -- Shock
			0.8, -- Burn
			0.8, -- Radiation
			0.8, -- Acid
			0.8, -- Explosion
}