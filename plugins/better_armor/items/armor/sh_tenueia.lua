ITEM.name = "Gilet pare-balles IIA"
ITEM.description = "Un gilet pare-balles protégeant des très petits calibres, l'un des plus légers mais aussi moins résistants gilets allant jusqu'au 38 acp ou 9mm de basse qualité."
ITEM.model = "models/weapons/armor/armor.mdl"
ITEM.width = 3
ITEM.armorAmount = 0
ITEM.gasmask = false -- It will protect you from bad air
ITEM.height = 3
ITEM.category = "armor"
ITEM.resistance = true -- This will activate the protection bellow
ITEM.damage = { -- It is scaled; so 100 damage * 0.8 will makes the damage be 80.
			0.9, -- Bullets
			0.9, -- Slash
			0.9, -- Shock
			0.9, -- Burn
			0.9, -- Radiation
			0.9, -- Acid
			0.9, -- Explosion
}