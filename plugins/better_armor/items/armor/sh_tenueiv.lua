ITEM.name = "Gilet pare-balles IV"
ITEM.description = "Un gilet pare-balles protégeant de la totalité des calibres intermédiaires, un gilet chers mais très efficace, allant jusqu'au 5.56x45 ou encore le 30-06."
ITEM.model = "models/weapons/armor/armor.mdl"
ITEM.width = 3
ITEM.armorAmount = 0
ITEM.gasmask = false -- It will protect you from bad air
ITEM.height = 3
ITEM.category = "armor"
ITEM.resistance = true -- This will activate the protection bellow
ITEM.damage = { -- It is scaled; so 100 damage * 0.8 will makes the damage be 80.
			0.60, -- Bullets
			0.60, -- Slash
			0.60, -- Shock
			0.60, -- Burn
			0.60, -- Radiation
			0.60, -- Acid
			0.60, -- Explosion
}