ITEM.name = "Gilet pare-balles IIIA"
ITEM.description = "Un gilet pare-balles protégeant des calibres intermédiaires, l'un des plus communs, allant jusqu'au 44 magnum ou calibre 12 slug."
ITEM.model = "models/weapons/armor/armor.mdl"
ITEM.width = 3
ITEM.armorAmount = 0
ITEM.gasmask = false -- It will protect you from bad air
ITEM.height = 3
ITEM.category = "armor"
ITEM.resistance = true -- This will activate the protection bellow
ITEM.damage = { -- It is scaled; so 100 damage * 0.8 will makes the damage be 80.
			0.7, -- Bullets
			0.7, -- Slash
			0.7, -- Shock
			0.7, -- Burn
			0.7, -- Radiation
			0.7, -- Acid
			0.7, -- Explosion
}