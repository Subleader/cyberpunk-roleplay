local PLUGIN = PLUGIN

function PLUGIN:EntityFireBullets(entity, bulletInfo)
	if (entity:IsPlayer()) then
		weapon = entity:GetActiveWeapon()
	end
	
	if (weapon and weapon:IsWeapon()) then
		if entity:IsPlayer() then
			local character = entity:GetCharacter()
			local inventory = character:GetInventory()
			local items = inventory:GetItems()
			for k, v in pairs(items) do
				if (v.base == "base_weapons" and v:GetData("equip") and v.class == weapon:GetClass()) then
					local originalDamage = bulletInfo.Damage
					local durability = v:GetData("Durability")
					local drainScale = 100 * 0.9
					if (durability) then
						bulletInfo.Damage = (originalDamage / 100) * durability
						bulletInfo.Spread = bulletInfo.Spread * (1 + (1 - ((1 / 100) * durability)))
					
						v:SetData("Durability", math.max(durability - (20 / drainScale)))
						if v:GetData("Durability") < 0 then
							v:SetData("Durability", 0)
							v:Unequip(entity, true)
							entity:Notify("Votre arme s'est enraillée")
						end
					end
				end
			end
		end
	end
end