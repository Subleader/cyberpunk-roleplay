PLUGIN.name = "Survival System"
PLUGIN.author = "ZeMysticalTaco"
PLUGIN.description = "A survival system consisting of hunger and thirst."

ix.config.Add("thirst_decay_speed", 300, "Set the time that a leg remains broken.", nil, {
	data = {min = 1, max = 1000, decimals = 0},
	category = "Needs"
})

ix.config.Add("thirst_decay_amount", 1, "Set the time that a leg remains broken.", nil, {
	data = {min = 1, max = 50, decimals = 0},
	category = "Needs"
})

ix.config.Add("hunger_decay_speed", 300, "Set the time that a leg remains broken.", nil, {
	data = {min = 1, max = 1000, decimals = 0},
	category = "Needs"
})

ix.config.Add("hunger_decay_amount", 1, "Set the time that a leg remains broken.", nil, {
	data = {min = 1, max = 50, decimals = 0},
	category = "Needs"
})

if SERVER then
	function PLUGIN:OnCharacterCreated(client, character)
		character:SetData("hunger", 100)
		character:SetData("thirst", 100)
	end

	function PLUGIN:PlayerLoadedCharacter(client, character)
		timer.Simple(0.25, function()
			client:SetLocalVar("hunger", character:GetData("hunger", 100))
			client:SetLocalVar("thirst", character:GetData("thirst", 100))
		end)
	end

	function PLUGIN:CharacterPreSave(character)
		local client = character:GetPlayer()

		if (IsValid(client)) then
			character:SetData("hunger", client:GetLocalVar("hunger", 0))
			character:SetData("thirst", client:GetLocalVar("thirst", 0))
		end
	end

	local playerMeta = FindMetaTable("Player")

	function playerMeta:SetHunger(amount)
		local char = self:GetCharacter()

		if (char) then
			char:SetData("hunger", amount)
			self:SetLocalVar("hunger", amount)
		end
	end

	function playerMeta:SetThirst(amount)
		local char = self:GetCharacter()

		if (char) then
			char:SetData("thirst", amount)
			self:SetLocalVar("thirst", amount)
		end
	end

	function playerMeta:TickThirst(amount)
		local char = self:GetCharacter()

		if (char) then
			char:SetData("thirst", char:GetData("thirst", 100) - amount)
			self:SetLocalVar("thirst", char:GetData("thirst", 100) - amount)

			if char:GetData("thirst", 100) < 0 then
				char:SetData("thirst", 0)
				self:SetLocalVar("thirst", 0)
			end
		end
	end

	function playerMeta:TickHunger(amount)
		local char = self:GetCharacter()

		if (char) then
			char:SetData("hunger", char:GetData("hunger", 100) - amount)
			self:SetLocalVar("hunger", char:GetData("hunger", 100) - amount)

			if char:GetData("hunger", 100) < 0 then
				char:SetData("hunger", 0)
				self:SetLocalVar("hunger", 0)
			end
		end
	end

	function PLUGIN:PlayerTick(ply)
		if ply:GetNetVar("hungertick", 0) <= CurTime() then
			ply:SetNetVar("hungertick", ix.config.Get("hunger_decay_speed", 300) + CurTime())
			ply:TickHunger(ix.config.Get("hunger_decay_amount", 1))
		end

		if ply:GetNetVar("thirsttick", 0) <= CurTime() then
			ply:SetNetVar("thirsttick", ix.config.Get("thirst_decay_speed", 300) + CurTime())
			ply:TickThirst(ix.config.Get("thirst_decay_amount", 1))
		end
	end
else
	ix.bar.Add(function()
		local status = ""
		local var = LocalPlayer():GetLocalVar("hunger", 0) / 100

		if var < 0.2 then
			status = "Je suis affamé"
		elseif var < 0.4 then
			status = "J'ai faim"
		elseif var < 0.6 then
			status = "J'ai un peu faim"
		elseif var < 0.8 then
			status = "Mon ventre gargouille"
		end

		return var, status
	end, Color(204, 102, 0), nil, "hunger")

	ix.bar.Add(function()
		local status = ""
		local var = LocalPlayer():GetLocalVar("thirst", 0) / 100

		if var < 0.2 then
			status = "Je suis déshydraté"
		elseif var < 0.4 then
			status = "Je suis un peu déshydraté"
		elseif var < 0.6 then
			status = "J'ai soif"
		elseif var < 0.8 then
			status = "J'ai besoin de me désaltérer"
		end

		return var, status
	end, Color(0, 51, 204), nil, "thirst")
end

local playerMeta = FindMetaTable("Player")

function playerMeta:GetHunger()
	local char = self:GetCharacter()

	if (char) then
		return char:GetData("hunger", 100)
	end
end

function playerMeta:GetThirst()
	local char = self:GetCharacter()

	if (char) then
		return char:GetData("thirst", 100)
	end
end

function PLUGIN:AdjustStaminaOffset(client, offset)
	if client:GetHunger() < 15 or client:GetThirst() < 20 then
		return -1
	end
end

--TODO: Populate Hunger and Thirst Items.
--TODO: Drown out colors and restrict stamina restoration for hungry / thirsty players.
local hunger_items = {
	["normal_win"] = {
		["name"] = "Bouteille de vin 75cl",
		["model"] = "models/foodnhouseholditems/wine_red1.mdl",
		["desc"] = "Une bonne bouteille de vin importée de l'Union.",
		["hunger"] = -5,
		["thirst"] = 25,
	},

	["normal_beer"] = {
		["name"] = "Bouteille de biere 50cl",
		["model"] = "models/foodnhouseholditems/beer_master.mdl",
		["desc"] = "Une bouteille de biere standard, fin c'est dégueulasse surtout celle-ci.",
		["hunger"] = -2,
		["thirst"] = 10,
	},

	["big_beer"] = {
		["name"] = "Canette de bierre 33cl",
		["model"] = "models/foodnhouseholditems/beercan02.mdl",
		["desc"] = "Une canette de biere, pratique mais peut être trop petit.",
		["hunger"] = -1,
		["thirst"] = 7,
	},

	["big_water"] = {
		["name"] = "Eau 2L",
		["model"] = "models/props_junk/garbage_plasticbottle003a.mdl",
		["desc"] = "Une grosse bouteille d'eau",
		["hunger"] = 2,
		["thirst"] = 100
	},

	["water"] = {
		["name"] = "Eau 75cl",
		["model"] = "models/props/cs_office/water_bottle.mdl",
		["desc"] = "Une bouteille d'eau",
		["hunger"] = 1,
		["thirst"] = 50
	},

	["soda"] = {
		["name"] = "Coca Cola",
		["model"] = "models/foodnhouseholditems/sodacan01.mdl",
		["desc"] = "C'est un coca, tout le monde connait",
		["hunger"] = 5,
		["thirst"] = 30
	},

	["soda"] = {
		["name"] = "Monster",
		["model"] = "models/foodnhouseholditems/sodacanb01.mdl",
		["desc"] = "Une bonne boisson d'homme, attention les enfants il ne faut pas en boire beaucoup",
		["hunger"] = 5,
		["thirst"] = 30
	},


	["coca"] = {
		["name"] = "Sprite",
		["model"] = "models/foodnhouseholditems/sodacan05.mdl",
		["desc"] = "Un sprite qui piquotte un peu",
		["hunger"] = 5,
		["thirst"] = 30
	},

	["ragout_chien"] = {
		["name"] = "Pomme",
		["model"] = "models/foodnhouseholditems/apple.mdl",
		["desc"] = "Une pomme, que dire de plus",
		["hunger"] = 20,
		["thirst"] = 5,
		["isnourr"] = true,
	},

	["ragout_ban"] = {
		["name"] = "Banane",
		["model"] = "models/foodnhouseholditems/bananna.mdl",
		["desc"] = "Une banane, que dire de plus",
		["hunger"] = 22,
		["thirst"] = 5,
		["isnourr"] = true,
	},

	["rasins"] = {
		["name"] = "Raisins",
		["model"] = "models/foodnhouseholditems/grapes3.mdl",
		["desc"] = "Une grape de raisins, que dire de plus",
		["hunger"] = 30,
		["thirst"] = 7,
		["isnourr"] = true,
	},

	["pizza"] = {
		["name"] = "Pizza",
		["model"] = "models/foodnhouseholditems/pizza.mdl",
		["desc"] = "Une bonne pizza Italienne faites aux Etats Unis...",
		["hunger"] = 50,
		["thirst"] = 0,
		["isnourr"] = true,
	},

	["saucisson"] = {
		["name"] = "Cheese Burger",
		["model"] = "models/foodnhouseholditems/burgersims2.mdl",
		["desc"] = "Rien de mieux qu'un bon cheese burger, après tout c'est la meilleure nourriture du monde!",
		["hunger"] = 40,
		["thirst"] = -5,
		["isnourr"] = true,
	},

	["conserver_rouge"] = {
		["name"] = "Cuisse de dinde",
		["model"] = "models/foodnhouseholditems/meat4.mdl",
		["desc"] = "Une bonne grosse cuisse de dinde",
		["hunger"] = 50,
		["thirst"] = -5,
		["isnourr"] = true,
	},

	["pot_rat"] = {
		["name"] = "Chips",
		["model"] = "models/foodnhouseholditems/chipslays8.mdl",
		["desc"] = "Des petites chips pour grignotter",
		["hunger"] = 15,
		["thirst"] = 0,
		["isnourr"] = true,
	},

	["pain"] = {
		["name"] = "Morceau de Pain",
		["model"] = "models/foodnhouseholditems/bread_half.mdl",
		["desc"] = "C'est sec, c'est sans odeur, mais ça se mange faut croire",
		["hunger"] = 20,
		["thirst"] = -5,
		["isnourr"] = true,
	},

	["pot_thon"] = {
		["name"] = "Paquet de cookies",
		["model"] = "models/foodnhouseholditems/cookies.mdl",
		["desc"] = "Avec ça, pas de surprise, c'est super bon, mais ça nourrit pas des masses",
		["hunger"] = 25,
		["thirst"] = -5,
		["isnourr"] = true,
	},


}

for k, v in pairs(hunger_items) do
	local ITEM = ix.item.Register(k, nil, false, nil, true)
	ITEM.name = v.name
	ITEM.description = v.desc
	ITEM.model = v.model
	ITEM.width = v.width or 1
	ITEM.height = v.height or 1
	ITEM.category = "Survival"
	ITEM.hunger = v.hunger or 0
	ITEM.thirst = v.thirst or 0
	ITEM.empty = v.empty or false
	ITEM.isnourr = v.isnourr or false
	function ITEM:GetDescription()
		return self.description
	end
	ITEM.functions.Consume = {
		name = "Consommer",
		OnRun = function(item)
			local hunger = item.player:GetCharacter():GetData("hunger", 100)
			local thirst = item.player:GetCharacter():GetData("thirst", 100)

			if thirst <= 100 then
				item.player:SetThirst(thirst + item.thirst)
				if thirst > 100 then
					item.player:SetThirst(100)
				end
			end
			if hunger <= 100 then
				item.player:SetHunger(hunger + item.hunger)
				if hunger > 100 then
					item.player:SetHunger(100)
				end
			end
			if item.isnourr then
				item.player:EmitSound("stalker/hgn/pl_food.wav")
			else
				item.player:EmitSound("stalker/hgn/pl_softdrink.wav")
			end
			if item.empty then
				local inv = item.player:GetCharacter():GetInventory()
				inv:Add(item.empty)
			end
		end
	}
end

ix.command.Add("SetHunger", {
	description = "Set la faim de quelqu'un.",
	adminOnly = true,
	arguments = {ix.type.character, ix.type.number},
	OnRun = function(self, client, target, hunger)
		if target then
			target.player:SetHunger(hunger)
			client:Notify("Vous avez mis la nourriture de " .. target:GetName() .. " à " .. hunger .. ".")
		end
	end
})

ix.command.Add("SetThirst", {
	description = "Set la soif de quelqu'un.",
	adminOnly = true,
	arguments = {ix.type.character, ix.type.number},
	OnRun = function(self, client, target, thirst)
		if target then
			target.player:SetThirst(thirst)
			client:Notify("Vous avez mis la nourriture de " .. target:GetName() .. " à " .. thirst .. ".")
		end
	end
})

