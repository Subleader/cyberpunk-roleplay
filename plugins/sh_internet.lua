local PLUGIN = PLUGIN

ix.chat.Register("tel", {
	format = "[Groupe] *%s* dit \"%s\"",
	color = Color(51, 153, 255, 255),
	deadCanChat = true,

	OnChatAdd = function(self, speaker, text, bAnonymous, data)
		chat.AddText(self.color, string.format(self.format, speaker:GetName(), text))
	end
})

ix.command.Add("Tel", {
	description = "Parler aux membres de votre escouade",
	arguments = {
		ix.type.text
	},
	OnRun = function(self, client, message)
		ix.chat.Send(client, "Tel", message, false)
	end
})

do
	local CLASS = {}
	CLASS.color = Color(51, 153, 255, 255)
	CLASS.format = "[Groupe] *Nils Rhoades* dit \"%s\""

	function CLASS:OnChatAdd(speaker, text)
		chat.AddText(self.color, string.format(self.format, text))
	end

	ix.chat.Register("nils", CLASS)
end

do
	local COMMAND = {}
	COMMAND.adminOnly = true
	COMMAND.arguments = ix.type.text

	function COMMAND:OnRun(client, message)
		ix.chat.Send(client, "nils", message)
	end

	ix.command.Add("Nils", COMMAND)
end
