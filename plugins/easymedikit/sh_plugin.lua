local PLUGIN = PLUGIN
PLUGIN.name = "EasyMedikit"
PLUGIN.desc = "A small base of medical kit (Heal yourself, heal others, works with medical attribute, adds pain)"
PLUGIN.author = "Subleader"

ix.util.Include("cl_plugin.lua")

function PLUGIN:EntityTakeDamage( target, dmginfo )
	if ( target:IsPlayer() and target:IsValid() ) then
		local random = math.random (1,8)
		if dmginfo:GetDamage() > 30 then
			target:SetNetVar("playerPain", true)
		elseif random == 1 then
			target:SetNetVar("playerPain", true)
		end
	end
end