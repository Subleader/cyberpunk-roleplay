ITEM.name = "Kit de soin"
ITEM.model = Model("models/Items/HealthKit.mdl")
ITEM.description = "Un kit contenant tout ce dont un médecin a besoin en plus d'un flacon de biogel."
ITEM.category = "Medical"
ITEM.price = 0
ITEM.healthPoint = 90
ITEM.medAttr = 10
ITEM.isPainkiller = true