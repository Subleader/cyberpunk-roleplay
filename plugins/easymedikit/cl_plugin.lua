local PLUGIN = PLUGIN

function PLUGIN:HUDPaint()
	if LocalPlayer():GetNetVar("playerPain") then
		surface.SetDrawColor(255,255,255,255)
		surface.SetMaterial(Material("hud/psyz8.png"))
		surface.DrawTexturedRect( ScrW()/1.05, ScrH()/1.35,  33, 35) --Icone clignement	
	end
end

function PLUGIN:RenderScreenspaceEffects()
	if LocalPlayer():GetNetVar("playerPain") then
		 DrawMotionBlur( 0.4, 0.8, 0.01 )
		 DrawSharpen( 0.8, 0.8 )
	end
end