local PLUGIN = PLUGIN

ix.command.Add("Doorunlock", {
	description = "Set the door unlocked",
	adminOnly = true,
	OnRun = function(self, client, target)
		local entity = client:GetEyeTrace().Entity
		
		if (IsValid(entity) and entity:IsDoor()) then
			entity:EmitSound("doors/door_latch3.wav");
			entity:Fire("Unlock", "", 0);
		else
		return "Not a valid door"
		end
	end
})

ix.command.Add("Doorlock", {
	description = "Set the door unlocked",
	adminOnly = true,
	OnRun = function(self, client, target)
		local entity = client:GetEyeTrace().Entity
		
		if (IsValid(entity) and entity:IsDoor()) then
			entity:EmitSound("doors/door_latch3.wav");
			entity:Fire("Lock", "", 0);
		else
			return "Not a valid door"
		end
	end
})

ix.command.Add("collection", {
	description = "Ouvrir la collection",
	adminOnly = false,
	OnRun = function(self, client)
		client:SendLua([[gui.OpenURL("https://steamcommunity.com/sharedfiles/filedetails/?id=1581715860")]])
	end
})

ix.command.Add("forum", {
	description = "Ouvrir le forum",
	adminOnly = false,
	OnRun = function(self, client)
		client:SendLua([[gui.OpenURL("https://hesperides.eu/forum/index.php")]])
	end
})

ix.command.Add("guide", {
	description = "Ouvrir le guide du serveur",
	adminOnly = false,
	OnRun = function(self, client)
		client:SendLua([[gui.OpenURL("https://steamcommunity.com/sharedfiles/filedetails/?id=1580754985")]])
	end
})

ix.command.Add("discord", {
	description = "Ouvrir le discord",
	adminOnly = false,
	OnRun = function(self, client)
		client:SendLua([[gui.OpenURL("https://discord.gg/beTEr2s")]])
	end
})

ix.command.Add("smotd", {
	description = "Ouvrir le motd",
	adminOnly = false,
	OnRun = function(self, client)
		umsg.Start( "sMotd", client ) -- Sending a message to the client.
		umsg.End()
	end
})