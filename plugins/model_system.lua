PLUGIN.name = "Two Models"
PLUGIN.author = "Subleader"
PLUGIN.description = "So a character can have two models."

ix.command.Add("CharSetModelCiv", {
	description = "Set le second model de quelqu'un.",
	adminOnly = true,
	arguments = {ix.type.character, ix.type.string},
	OnRun = function(self, client, target, model)
		if target then
			target.player:SetModel(model)
			target.player:SetupHands()
			target:SetData("modelCiv", model)
			target.player:SetNetVar("IsCiv", true)
		end
	end
})

ix.command.Add("Swap", {
	description = "Passer du model civil au militaire et l'inverse.",
	adminOnly = false,
	OnRun = function(self, client)
		character = client:GetCharacter()
		modelChar = character:GetModel()
		modelCur = client:GetModel()

		if character:GetData("modelCiv") then
			modelCiv = character:GetData("modelCiv")
			if modelChar == modelCur then -- Si le modele du character stored est egal au model du joueur
				client:SetModel(modelCiv)
				client:SetupHands()

				local bodygroup = client:GetBodyGroups()
				local bodygrouptest = client:GetCharacter():GetData("groupsCiv", {})
				for k, v in pairs( bodygroup ) do
						if bodygrouptest[k] then
							client:SetBodygroup(k, bodygrouptest[k])
						else
							client:SetBodygroup(k, 0)
						end
				end
				local skinModel = client:GetCharacter():GetData("skinCiv", 0)
				client:SetSkin(skinModel)
				client:SetNetVar("IsCiv", true)
				client:Notify("Vous etes passé au model civil")
			else
				client:SetModel(modelChar)
				client:SetupHands()

				local bodygroup = client:GetBodyGroups()
				local bodygrouptest = client:GetCharacter():GetData("groups", {})
				for k, v in pairs( bodygroup ) do
						if bodygrouptest[k] then
							client:SetBodygroup(k, bodygrouptest[k])
						else
							client:SetBodygroup(k, 0)
						end
				end
				local skinModel = client:GetCharacter():GetData("skin", 0)
				client:SetSkin(skinModel)
				client:SetNetVar("IsCiv", false)
				client:Notify("Vous etes passé au model militaire")
			end
		else
			client:Notify("Vous n'avez pas de deuxième model")
		end
	end
})